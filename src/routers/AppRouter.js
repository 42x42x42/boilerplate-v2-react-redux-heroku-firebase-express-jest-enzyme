import React from 'react';
import {Router, Route, Switch, Link, NavLink} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory'
import DashboardPage from '../components/DashboardPage';
import NotFoundPage from '../components/NotFoundPage';
import LoginPage from '../components/LoginPage';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

export const history = createHistory(); // требует установки npm i history

const AppRouter = () => (
    <Router history={history}>
        <div>
            {/*то что не входит в switch отображается на каждой странице,тут могут быть компоненты футеры,хедеры*/}
            {/*switch ищет первое сходство в пути и останавливается на нем*/}
            <Switch>
                <PublicRoute path="/" exact={true} component={LoginPage}/>
                <PrivateRoute path="/dashboard" component={DashboardPage}/>
                <Route component={NotFoundPage}/>
            </Switch>
        </div>
    </Router>
);
export default AppRouter;