import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'; // helps to provide single store to all app components
import AppRouter, {history} from './routers/AppRouter';
import configureStore from './redux/store/configureStore';
import {login, logout} from './redux/actions/auth-Action';
import {firebase} from './firebase/firebase';
import 'normalize.css/normalize.css';
// react-dates@12.7.0 (!!именно эта версия)для работы с календарем, библиотека от airbnb,требует  npm i react-addons-shallow-compare
// и импортировать import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/lib/css/_datepicker.css';
import './styles/styles.scss';
import LoadingPage from './components/LoadingPage';


const appRoot = document.getElementById("app");
const STORE = configureStore();
// достаточно просто обернуть основной компонент в Провайдер указав ему в параметры ссылку на Store объявленый выше
// и у компонентов будет общий стор
// для этого в каждом компоненте где нужен доступ к Стору необходимо вызывать функцию connect
const jsx = (
    <Provider store={STORE}>
        <AppRouter/>
    </Provider>
);

let hasRendered = false;
const renderApp = () => {
    if (!hasRendered) {
        ReactDOM.render(jsx, appRoot);
        hasRendered = true;
    }
};
ReactDOM.render(<LoadingPage/>, appRoot);

// после ответа от firebase грузится экран с логином либо пользовательскими данными
firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        console.log('Log IN');
        // страница юзера
        STORE.dispatch(login(user.uid)); //   firebase передает uid самостоятельно
        renderApp();
        // если вход произошел со страницы "логин" она же "/" только тогда пересылать на /dashboard
        if (history.location.pathname === '/') {
            history.push('/dashboard');
        }
    } else {
        console.log('Log out');
        STORE.dispatch(logout());
        // если юзер не залогинен то показать ему странинцу логина по адресу "/"
        renderApp();
        history.push('/');
    }
});



