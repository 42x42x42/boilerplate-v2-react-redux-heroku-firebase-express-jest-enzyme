import React from 'react';
import {startLogin} from '../redux/actions/auth-Action';

export const LoginPage = ({startLogin}) => (
    <div className="box-layout">
        <div className="box-layout__box">
            <h1 className="box-layout__title">Boilerplate</h1>
            <p>It's time to take your expenses under control</p>
            <button onClick={startLogin} className="button">Login with Google</button>
        </div>
    </div>
);


import {connect} from 'react-redux';

const mapDispatchToProps = (dispatch) => ({
    startLogin: () => dispatch(startLogin())
});

export default connect(undefined, mapDispatchToProps)(LoginPage);
