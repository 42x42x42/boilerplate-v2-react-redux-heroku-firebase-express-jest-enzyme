import * as firebase from 'firebase';


// for heroku variables set via command line and 'heroku config:set ' command
// original keys are hidden from git
const config = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID
};
firebase.initializeApp(config);
const database = firebase.database();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export {firebase,googleAuthProvider, database as default};





// -- Примеры команд в firebase --

// database.ref().set({
//     name: "Dmytro",
//     age: 28,
//     isSingle: false,
//     stresslevel:6,
//     job :{
//         title: 'software developer',
//         company : 'google'
//     },
//     location: {
//         city: "New York",
//         country: "USA"
//     }
// }).then(() => console.log('data is set 1')) // -- возвращает промис
//     .catch((e) => console.log(e));

// database.ref("age").set(29);
// database.ref('location/city').set("Copenhaven");
// database.ref("attributes").set({
//     height: 180,
//     weight: 70
// }).then(() => console.log('data is set 2')).catch(e => console.log(e));

// -- удаление
// database.ref('isSingle').remove();

// -- обновление
// database.ref().update({
//     stresslevel: 9,
//     'job/company': 'amazon', // -- если есть вложенность то нужно указывать путь через / и в сбоки брать,иначе сотрет все что вложено,но не вошло в апдейт
//     'location/city': 'Boston'
// });


// -- разовые запрос на получение базы
// database.ref()
//     .once('value')
//     .then(snapshot => console.log(snapshot.val())) // .val() чтоб получить именно сами значения, а не все вспомогательные функции
//     .catch(e => console.log(e));

// -- подписывает слушатель на изменения базы, принимает стринг что слушать и функцию что делать с полученными данными(к примеру обновить компонент)
// -- возвращает фунцию для отписки
// -- Типы слушателей могут быть value,child_changed, child_removed,child_added and so on
// const onValueChange = database.ref().on('value', (snapshot)=>{
//     console.log(snapshot.val());
// });

// -- отписка от обновления
// database.ref().off(onValueChange);


// ---получение данных с сервера и их трансформация в Array
// database.ref('expenses').once('value').then((snapshot) => {
//     const expensesArray = [];
//
//     snapshot.forEach((childSnapshot) => {
//         expensesArray.push({
//             id: childSnapshot.key, //через push каждому объекту дан ключ\ид
//             ...childSnapshot.val()
//         })
//     });
//     console.log(expensesArray);
// });
//


// -- Добавляет данные в firebase храня их как ключ-значение (ключ дает автоматически)
// database.ref('expenses').push({description:'first', amount: 1, note:"", createdAt: 2345345});
// database.ref('expenses').push({description:'second', amount: 20, note:"", createdAt: 164804});
// database.ref('expenses').push({description:'third', amount: 30, note:"", createdAt: 98545});


// -- Подписка на обновления, преобразование данных firebase в массива из объектов, выимка ключа данного при push
// const onValueChanged = database.ref('expenses').on('value', (snapshot) => {
//     const expensesArray = [];
//     snapshot.forEach((childSnapshot) => {
//         expensesArray.push({
//             id: childSnapshot.key, //через push каждому объекту дан ключ\ид
//             ...childSnapshot.val()
//         })
//     });
//     console.log(expensesArray);
// });


